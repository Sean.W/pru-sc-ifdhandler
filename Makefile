CFLAGS= -pthread -I/usr/local/include/PCSC  
LDFLAGS= -L/usr/local/lib -lpcsclite  
CC=gcc

SOURCES=ifdhandler.c utils.c

all:	libtiserial.so

libtiserial.so: ${SOURCES}
	${CC} -o libtiserial.so ${SOURCES} -fPIC -D_REENTRANT -Wall -I. ${CFLAGS} ${LDFLAGS} -shared

clean-all:	clean
	rm Makefile.inc || true

clean:
	rm -f *~ *.o *.so || true

