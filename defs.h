/*
 * defs.h
 *
 * This file has all the data structures maintained to support multiple 
 * PRU based Smart Card readers and other error messages
 * 
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <pcsclite.h>

#define TI_DRIVER_MAX_READERS        1
#define MAX_READER_NAME_LEN          255
#define READER_NUM_INDEX             10
#define READER_COMMAND_FILE          "/sys/devices/platform/ti_omapl_pru_suart.1/sc_reader%d_cmd"
#define TI_READER_NAME               "/dev/ttySU%d"
#define TI_MAX_CMD_LEN               5
#define TI_MAX_ATR_LEN               18

typedef struct TIREADERDATA 
{
	char  readerName[MAX_READER_NAME_LEN];
	char  readerCmd[MAX_READER_NAME_LEN];
	int   readerFd; 
	int   commandFd; 

	int   lun;
	int   nATRLength;
	UCHAR pcATRBuffer[MAX_ATR_SIZE];

	UCHAR bPowerFlags;
} TiReaderData;

/* IFD Commands */
#define IFD_CMD_ICC_PRESENT      0x30
#define IFD_CMD_ICC_POWERUP      0x31    
#define IFD_CMD_ICC_POWERDOWN    0x32      
#define IFD_CMD_ICC_POWERRESET   0x33
#define IFD_CMD_ICC_ENABLEVCC    0x34
#define IFD_CMD_ICC_ENABLERESET  0x35
#define IFD_CMD_ICC_ISVCCENABLED 0x36

/* Response messages for IFD Commnds */
#define IFD_CMD_RSP_INPROGRESS   0xFF
#define IFD_CMD_RSP_SUCCESS      0x40
#define IFD_CMD_RSP_FAIL         0x41
#define IFD_CMD_RSP_NOTSUPPORTED 0x42

typedef enum {
	STATUS_NO_SUCH_DEVICE        = 0xF9,
	STATUS_SUCCESS               = 0xFA,
	STATUS_UNSUCCESSFUL          = 0xFB,
	STATUS_COMM_ERROR            = 0xFC,
	STATUS_DEVICE_PROTOCOL_ERROR = 0xFD,
	STATUS_COMM_NAK              = 0xFE,
	STATUS_SECONDARY_SLOT        = 0xFF
} status_t;

/* Powerflag (used to detect quick insertion removals unnoticed by the
 * resource manager) */
/* Initial value */
#define POWERFLAGS_RAZ 0x00
/* Flag set when a power up has been requested */
#define MASK_POWERFLAGS_PUP 0x01
/* Flag set when a power down is requested */
#define MASK_POWERFLAGS_PDWN 0x02

/* Communication buffer size (max=adpu+Lc+data+Le)
 * we use a 64kB for extended APDU on APDU mode readers */
#define CMD_BUF_SIZE (4 +3 +64*1024 +3)

/* Protocols */
#define T_0 0
#define T_1 1

/* Default communication read timeout in milliseconds */
#define DEFAULT_COM_READ_TIMEOUT (2*1000)

/*
 * communication ports abstraction
 */

status_t OpenSerial(unsigned int reader_index, unsigned int channel);

status_t OpenSerialByName(unsigned int reader_index, char *dev_name);

status_t SetBaudRate(unsigned int reader_index, unsigned int baud); 

status_t InitializeSerial(unsigned int reader_index, int baud, int bits, char parity); 

status_t WriteSerial(unsigned int reader_index, unsigned int length,
	unsigned char *Buffer);

status_t ReadSerial(unsigned int reader_index, unsigned int *length,
	unsigned char *Buffer);

status_t ReadSerialNoWait(unsigned int reader_index, unsigned int *length,
	unsigned char *Buffer);

status_t CloseSerial(unsigned int reader_index);

int ExecCommandAndGetResult(unsigned int reader_index, int command);
