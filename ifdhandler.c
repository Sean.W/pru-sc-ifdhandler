/*
 * ifdhandler.c
 *
 * This is the basic file which has all the IFD Handlers for exchangin
 * data with the PRU Smart Card Reader.
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pcsclite.h>
#include <ifdhandler.h>
#include <reader.h>
#include "utils.h"
#include "defs.h"
#include "misc.h"
#include <unistd.h>

/* Array of structures to hold the ATR and other state value of each Reader */
TiReaderData readerData[TI_DRIVER_MAX_READERS];
static int driver_initialized = FALSE;


EXTERNAL RESPONSECODE IFDHCreateChannel(DWORD Lun, DWORD Channel)
{
	RESPONSECODE return_value = IFD_SUCCESS;
	int reader_index;

	if ( !driver_initialized )
		InitTiReaderData();

	if (-1 == (reader_index = GetNewTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	/* Reset ATR buffer */
	readerData[reader_index].nATRLength = 0;
	memset(readerData[reader_index].pcATRBuffer, 0, MAX_ATR_SIZE);

	/* Reset PowerFlags */
	readerData[reader_index].bPowerFlags = POWERFLAGS_RAZ;

	if (OpenSerial(reader_index, Channel) != STATUS_SUCCESS)
	{
		printf("failed");
		return_value = IFD_COMMUNICATION_ERROR;

		/* release the allocated reader_index */
		ReleaseTiReaderIndex(reader_index);
	}
	else
	{
#if 0
		unsigned char pcbuffer[SIZE_GET_SLOT_STATUS];
		unsigned int oldReadTimeout;

		/* Try to access the reader */
		/* This "warm up" sequence is sometimes needed when pcscd is
		 * restarted with the reader already connected. We get some
		 * "usb_bulk_read: Resource temporarily unavailable" on the first
		 * few tries. It is an empirical hack */
		if ((IFD_COMMUNICATION_ERROR == CmdGetSlotStatus(reader_index, pcbuffer))
			&& (IFD_COMMUNICATION_ERROR == CmdGetSlotStatus(reader_index, pcbuffer))
			&& (IFD_COMMUNICATION_ERROR == CmdGetSlotStatus(reader_index, pcbuffer)))
		{
			DEBUG_CRITICAL("failed");
			return_value = IFD_COMMUNICATION_ERROR;

			/* release the allocated resources */
			(void)ClosePort(reader_index);
			ReleaseReaderIndex(reader_index);
		}
#endif
	}

	return return_value;
} /* IFDHCreateChannel */

EXTERNAL RESPONSECODE IFDHCreateChannelByName(DWORD Lun, LPSTR lpcDevice)
{
	RESPONSECODE return_value = IFD_SUCCESS;
	int          reader_index;
	status_t     ret;

	/* First time when the driver is loaded, initialize all the
	   Reader data structure */
	if ( !driver_initialized )
		InitTiReaderData();

	/* Allocate an inded in the Reader data structure for the
	   new reader if it is not already in use */
	if (-1 == (reader_index = GetNewTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	/* Reset ATR buffer */
	readerData[reader_index].nATRLength = 0;
	memset(readerData[reader_index].pcATRBuffer, 0, MAX_ATR_SIZE);

	/* Reset PowerFlags */
	readerData[reader_index].bPowerFlags = POWERFLAGS_RAZ;

	/* reader name */
	strcpy(readerData[reader_index].readerName, lpcDevice);

	ret = OpenSerialByName(reader_index, lpcDevice);
	if (ret != STATUS_SUCCESS)
	{
		if (STATUS_NO_SUCH_DEVICE == ret)
			return_value = IFD_NO_SUCH_DEVICE;
		else
			return_value = IFD_COMMUNICATION_ERROR;

		/* release the allocated reader_index */
		ReleaseTiReaderIndex(reader_index);
	}
	else
	{
#if 0
		unsigned char pcbuffer[SIZE_GET_SLOT_STATUS];
		unsigned int oldReadTimeout;

		/* Try to access the reader */
		/* This "warm up" sequence is sometimes needed when pcscd is
		 * restarted with the reader already connected. We get some
		 * "usb_bulk_read: Resource temporarily unavailable" on the first
		 * few tries. It is an empirical hack */
		if ((IFD_COMMUNICATION_ERROR == CmdGetSlotStatus(reader_index, pcbuffer))
				&& (IFD_COMMUNICATION_ERROR == CmdGetSlotStatus(reader_index, pcbuffer))
				&& (IFD_COMMUNICATION_ERROR == CmdGetSlotStatus(reader_index, pcbuffer)))
		{
			DEBUG_CRITICAL("failed");
			return_value = IFD_COMMUNICATION_ERROR;

			/* release the allocated resources */
			CloseSerial(reader_index);
			ReleaseTiReaderIndex(reader_index);
		}
#endif
	}

	return return_value;
} /* IFDHCreateChannelByName */

EXTERNAL RESPONSECODE IFDHCloseChannel(DWORD Lun)
{
	int reader_index;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	if( ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_POWERDOWN) != IFD_CMD_RSP_SUCCESS)
		printf("Error trying to power down the ICC");
	/* No reader status check, if it failed, what can you do ? :) */

	(void)CloseSerial(reader_index);
	ReleaseTiReaderIndex(reader_index);

	return IFD_SUCCESS;
} /* IFDHCloseChannel */

EXTERNAL RESPONSECODE IFDHGetCapabilities(DWORD Lun, DWORD Tag, PDWORD Length, PUCHAR Value)
{
	int reader_index;
	RESPONSECODE return_value = IFD_SUCCESS;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	switch (Tag)
	{
		case TAG_IFD_ATR:
		case SCARD_ATTR_ATR_STRING:
			/* If Length is not zero, powerICC has been performed.
			 * Otherwise, return NULL pointer
			 * Buffer size is stored in *Length */
			if ((int)*Length >= readerData[reader_index].nATRLength)
			{
				*Length = readerData[reader_index].nATRLength;

				memcpy(Value, readerData[reader_index].pcATRBuffer, *Length);
			}
			break;

		case SCARD_ATTR_ICC_INTERFACE_STATUS:
			*Length = 1;
			if (IFD_ICC_PRESENT == IFDHICCPresence(Lun))
				/* nonzero if contact is active */
				*Value = 1;
			else
				/* smart card electrical contact is not active */
				*Value = 0;
			break;

		case SCARD_ATTR_ICC_PRESENCE:
			*Length = 1;
			/* Single byte indicating smart card presence:
			 * 0 = not present
			 * 1 = card present but not swallowed (applies only if
			 *     reader supports smart card swallowing)
			 * 2 = card present (and swallowed if reader supports smart
			 *     card swallowing)
			 * 4 = card confiscated. */
			if (IFD_ICC_PRESENT == IFDHICCPresence(Lun))
				/* Card present */
				*Value = 2;
			else
				/* Not present */
				*Value = 0;
			break;

		case TAG_IFD_SIMULTANEOUS_ACCESS:
			*Length = 1;
			*Value = TI_DRIVER_MAX_READERS;
			break;

		case TAG_IFD_SLOTS_NUMBER:
			*Length = 1;
			*Value = 1;
			break;

		case TAG_IFD_THREAD_SAFE:
			if (*Length >= 1) {
				*Length = 1;
				*Value = 1;
			}
			break;

		default:
			return_value = IFD_ERROR_TAG;
	}

	return return_value;
} /* IFDHGetCapabilities */


EXTERNAL RESPONSECODE IFDHSetCapabilities(DWORD Lun, DWORD Tag,
	/*@unused@*/ DWORD Length, /*@unused@*/ PUCHAR Value)
{
	(void)Length;
	(void)Value;

	int reader_index;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	return IFD_NOT_SUPPORTED;
} /* IFDHSetCapabilities */


EXTERNAL RESPONSECODE IFDHSetProtocolParameters(DWORD Lun, DWORD Protocol,
	UCHAR Flags, UCHAR PTS1, UCHAR PTS2, UCHAR PTS3)
{
	return IFD_SUCCESS;
} /* IFDHSetProtocolParameters */


EXTERNAL RESPONSECODE IFDHPowerICC(DWORD Lun, DWORD Action, PUCHAR Atr, PDWORD AtrLength)
{
	unsigned int nlength;
	RESPONSECODE return_value = IFD_SUCCESS;
	unsigned char pcbuffer[10+MAX_ATR_SIZE] = {0};
	int reader_index;

	/* By default, assume it won't work :) */
	*AtrLength = 0;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	switch (Action)
	{
		case IFD_POWER_DOWN:
			/* Clear ATR buffer */
			readerData[reader_index].nATRLength = 0;
			memset(readerData[reader_index].pcATRBuffer, 0, MAX_ATR_SIZE);

			/* Memorise the request */
			readerData[reader_index].bPowerFlags |= MASK_POWERFLAGS_PDWN;

			/* send the command */
			if (IFD_CMD_RSP_SUCCESS != ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_POWERDOWN))
			{
				printf("PowerDown failed");
				return_value = IFD_ERROR_POWER_ACTION;
				goto end;
			}

			break;

		case IFD_POWER_UP:
		case IFD_RESET:
			nlength = sizeof(pcbuffer);

			/* If the command is reset, then check whether the VCC is enabled.
			   If not ebabled, then do the action like POWER_UP else execute the IFD_CMD_ICC_POWERRESET command */
			if(Action == IFD_RESET) {
				if(IFD_CMD_RSP_SUCCESS != ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_ISVCCENABLED))
					Action = IFD_POWER_UP;
				else {
					if(IFD_CMD_RSP_SUCCESS != ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_POWERRESET)) {
						return_value = IFD_ERROR_POWER_ACTION;
						goto end;
					}
				}
			}

			if(Action == IFD_POWER_UP) {
				if(IFD_CMD_RSP_SUCCESS != ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_ENABLEVCC))
					return_value = IFD_ERROR_POWER_ACTION;

				// This is to flush the invalid characters any after enabling the VCC
				ReadSerialNoWait(reader_index, &nlength, pcbuffer);
				nlength = sizeof(pcbuffer);

				if(IFD_CMD_RSP_SUCCESS != ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_ENABLERESET))
					return_value = IFD_ERROR_POWER_ACTION;
			}

			if (return_value != IFD_SUCCESS)
			{
				printf("PowerUp failed");
				goto end;
			}

			/* Power up successful, set state variable to memorise it */
			readerData[reader_index].bPowerFlags |= MASK_POWERFLAGS_PUP;
			readerData[reader_index].bPowerFlags &= ~MASK_POWERFLAGS_PDWN;

			if(ReadSerial(reader_index, &nlength, pcbuffer) != STATUS_SUCCESS)
				return_value = IFD_COMMUNICATION_ERROR; 

			/* Reset is returned, even if TCK is wrong */
			readerData[reader_index].nATRLength = *AtrLength =
				(nlength < MAX_ATR_SIZE) ? nlength : MAX_ATR_SIZE;
			memcpy(Atr, pcbuffer, *AtrLength);
			memcpy(readerData[reader_index].pcATRBuffer, pcbuffer, *AtrLength);

			break;

		default:
			printf("Action not supported");
			return_value = IFD_NOT_SUPPORTED;
	}
end:

	return return_value;
} /* IFDHPowerICC */

EXTERNAL RESPONSECODE IFDHTransmitToICC(DWORD Lun, SCARD_IO_HEADER SendPci,
	PUCHAR TxBuffer, DWORD TxLength,
	PUCHAR RxBuffer, PDWORD RxLength, /*@unused@*/ PSCARD_IO_HEADER RecvPci)
{
	RESPONSECODE return_value = IFD_SUCCESS;
	unsigned int rx_length    = 0;
	unsigned int TxDoneLenght = 0;
	unsigned int RxDoneLenght = 256;
	unsigned char ins_byte    = 0;
	int         reader_index  =-1;

	(void)RecvPci;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	if(SendPci.Protocol != T_0)
		return_value = IFD_PROTOCOL_NOT_SUPPORTED;
	else {

		/* This is to flush the invalid characters any in the FIFO */
		ReadSerialNoWait(reader_index, &RxDoneLenght, RxBuffer);

#define COMMAND_HDR_LEN         5
#define COMMAND_INS_OFFSET      1
#define PROCEDURE_NULL          0x60
#define PROCEDURE_SW1_6X_LOW    0x61
#define PROCEDURE_SW1_6X_HIGH   0x6F
#define PROCEDURE_SW1_9X_LOW    0x90
#define PROCEDURE_SW1_9X_HIGH   0x9F

		/* Take the ins_byte from the Command Header */
		ins_byte = TxBuffer[COMMAND_INS_OFFSET];

		/* Send the Command Header first */
		if(WriteSerial(reader_index, ((TxLength > COMMAND_HDR_LEN)? COMMAND_HDR_LEN : TxLength), TxBuffer) != STATUS_SUCCESS)
			return_value = IFD_COMMUNICATION_ERROR; 

		TxDoneLenght = (TxLength > COMMAND_HDR_LEN)? COMMAND_HDR_LEN : TxLength;

		while(return_value == IFD_SUCCESS) {
			/* Read The Procedure Byte */
			RxDoneLenght = 1;
			if(ReadSerial(reader_index, &RxDoneLenght, RxBuffer) != STATUS_SUCCESS) {
				return_value = IFD_RESPONSE_TIMEOUT; 
				break;
			}

			/* If the procedure Byte is PROCEDURE_NULL, 
			   then wait for the procedure byte again */
			if(RxBuffer[0] == PROCEDURE_NULL)
				continue;

			/* If the procedure Byte is INS byte, 
			   then send the rest of the data and wiat for response/procedure byte */
			if(RxBuffer[0] == ins_byte) {
	
				RxDoneLenght = 0;

				if(TxLength > COMMAND_HDR_LEN) {
					if(WriteSerial(reader_index, TxLength-TxDoneLenght, &TxBuffer[TxDoneLenght]) != STATUS_SUCCESS) {
						return_value = IFD_COMMUNICATION_ERROR; 
						break;
					}
					/* All the data transmitted, so just read the
					   response and send it back */
					TxDoneLenght = TxLength;
					continue;
				}
				else
					break;
			}

			/* If the procedure Byte is INS^0xFF , 
			   then send the next 1 byte and wait for the procedure byte */
			if(RxBuffer[0] == (ins_byte ^ 0xFF)) {
				if(WriteSerial(reader_index, 1, &TxBuffer[TxDoneLenght]) != STATUS_SUCCESS) {
					return_value = IFD_COMMUNICATION_ERROR; 
					break;
				}

				TxDoneLenght += 1;
				RxDoneLenght = 0;

				/* If Everything is sent, then come out and wait 
				   for the response/procedure byte */
				continue;
			}

			/* If the procedure byte is SW1 byte, then go out of loop,
			   the code logic will receive the SW2 byte */
			if( ((RxBuffer[0] >= PROCEDURE_SW1_6X_LOW) && (RxBuffer[0] <= PROCEDURE_SW1_6X_HIGH)) ||
			    ((RxBuffer[0] >= PROCEDURE_SW1_9X_LOW) && (RxBuffer[0] <= PROCEDURE_SW1_9X_HIGH)) ) {
				RxDoneLenght = 1;
				break;
			}

			/* If the Procedure byte is not matching any, then treat it as error */
			return_value = IFD_COMMUNICATION_ERROR;
			break;
		}

		rx_length = *RxLength;
		if(return_value == IFD_SUCCESS)
			if(ReadSerial(reader_index, &rx_length, &RxBuffer[RxDoneLenght]) != STATUS_SUCCESS)
				return_value = IFD_COMMUNICATION_ERROR; 

		if (IFD_SUCCESS == return_value)
			*RxLength = rx_length + RxDoneLenght;
		else
			*RxLength = 0;

	}

	return return_value;
} /* IFDHTransmitToICC */

#define IOCTL_SMARTCARD_RDR_SET_BAUDRATE SCARD_CTL_CODE(2052)

EXTERNAL RESPONSECODE IFDHControl(DWORD Lun, DWORD dwControlCode,
	PUCHAR TxBuffer, DWORD TxLength, PUCHAR RxBuffer, DWORD RxLength,
	PDWORD pdwBytesReturned)
{
	int reader_index;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	if(IOCTL_SMARTCARD_RDR_SET_BAUDRATE == dwControlCode) {
	   unsigned int *baudRate = (unsigned int *) TxBuffer;
	   if(SetBaudRate(reader_index, *baudRate) != IFD_SUCCESS)
		return IFD_NOT_SUPPORTED;
	   else
		return IFD_SUCCESS;
	}
	
	return IFD_NOT_SUPPORTED;
} /* IFDHControl */

EXTERNAL RESPONSECODE IFDHICCPresence(DWORD Lun)
{
	int reader_index;
	RESPONSECODE return_value = IFD_SUCCESS;

	if (-1 == (reader_index = LunToTiReaderIndex(Lun)))
		return IFD_COMMUNICATION_ERROR;

	if(IFD_CMD_RSP_SUCCESS == ExecCommandAndGetResult(reader_index, IFD_CMD_ICC_PRESENT))
		return_value = IFD_ICC_PRESENT;
	else
		return_value = IFD_ICC_NOT_PRESENT;

	return return_value;
} /* IFDHICCPresence */

