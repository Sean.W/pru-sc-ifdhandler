/*
 * utils.c
 *
 * This is the additional functions to that will help accomplish the job 
 * in IFD handlers. This has the functions to open, read, write and close
 * serial port etc.
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


#include <pcsclite.h>
#include "defs.h"
#include "utils.h"

extern TiReaderData readerData[TI_DRIVER_MAX_READERS];

void InitTiReaderData(void)
{
	int i;

	memset(readerData, 0, sizeof(readerData));
	for (i = 0; i < TI_DRIVER_MAX_READERS; i++) {
		readerData[i].lun       = -1;
		readerData[i].readerFd  = -1;
	}
	return;

} /* InitTiReaderData */

int GetNewTiReaderIndex(const int Lun)
{
	int i;

	/* check that Lun is NOT already used */
	for (i = 0; i < TI_DRIVER_MAX_READERS; i++)
		if (readerData[i].lun == Lun)
			break;

	if (i < TI_DRIVER_MAX_READERS) {
		printf("Lun: %d is already used", Lun);
		return -1;
	}

	/* Allocate the first available index */
	for (i = 0; i < TI_DRIVER_MAX_READERS; i++) {
		if ( readerData[i].lun == -1 ) {
			readerData[i].lun = Lun;
			return i;
		}
	}

	printf("readerData[] is full");
	return -1;
} /* GetTiReaderIndex */

int LunToTiReaderIndex(const int Lun)
{
	int i;

	for (i = 0; i < TI_DRIVER_MAX_READERS; i++)
		if (readerData[i].lun == Lun) {
			return i;
		}

	printf("Lun: %X not found", Lun);
	return -1;
} /* LunToTiReaderIndex */

void ReleaseTiReaderIndex(const int index)
{
	memset(&readerData[index], 0, sizeof(TiReaderData));
	readerData[index].lun       = -1;
	readerData[index].readerFd  = -1;
	return;
} /* ReleaseTiReaderIndex */



int ExecCommandAndGetResult(unsigned int reader_index, int command)
{
	unsigned char cmdBuf[TI_MAX_CMD_LEN] = {command, };
	unsigned char rspBuf[TI_MAX_CMD_LEN] = {0, };
	int           commandFd = -1;

	if((commandFd = open(readerData[reader_index].readerCmd, O_RDWR | O_NOCTTY)) == -1)
		return -1;
	if(write(commandFd, cmdBuf, 1) == -1) {
		printf("write command failed %s", strerror(errno));
		close(commandFd);
		return -1;
	}
	close(commandFd);

	/* Read the status */
	if((commandFd = open(readerData[reader_index].readerCmd, O_RDWR | O_NOCTTY)) == -1)
		return -1;
	if(read(commandFd, rspBuf, 1) == -1) {
		printf("read command failed %s", strerror(errno));
		close(commandFd);
		return -1;
	}
	close(commandFd);

	/* If the command is in progress, then wait for 10 milli secs
	   and try to read the status again */
	if(rspBuf[0] != IFD_CMD_RSP_INPROGRESS)
		return (int)rspBuf[0];

	usleep(10000);

	/* Second Try */
	if((commandFd = open(readerData[reader_index].readerCmd, O_RDWR | O_NOCTTY)) == -1)
		return -1;
	if(read(commandFd, rspBuf, 1) == -1) {
		printf("read command failed %s", strerror(errno));
		close(commandFd);
		return -1;
	}
	close(commandFd);

	if(rspBuf[0] != IFD_CMD_RSP_INPROGRESS)
		return (int)rspBuf[0];

	usleep(10000);

	/* Third Try, This is the last one */
	if((commandFd = open(readerData[reader_index].readerCmd, O_RDWR | O_NOCTTY)) == -1)
		return -1;
	if(read(commandFd, rspBuf, 1) == -1) {
		printf("read command failed %s", strerror(errno));
		close(commandFd);
		return -1;
	}
	close(commandFd);

	return (int)rspBuf[0];
}


status_t OpenSerialByName(unsigned int reader_index, char *dev_name)
{
	char   readerCmdFile[MAX_READER_NAME_LEN] = {0};
	int    readerNum = dev_name[READER_NUM_INDEX] - 0x34;

	if(readerNum > TI_DRIVER_MAX_READERS) {
		printf("open %s: %s\n", dev_name, "Invalid Reader Number");
		return STATUS_NO_SUCH_DEVICE;
	}

	readerData[reader_index].readerFd = open(dev_name, O_RDWR | O_NOCTTY);

	if (-1 == readerData[reader_index].readerFd)
	{
		printf("open %s: %s", dev_name, strerror(errno));
		return STATUS_NO_SUCH_DEVICE;
	}
	
	sprintf(readerCmdFile, READER_COMMAND_FILE, readerNum); 
        strcpy(readerData[reader_index].readerCmd, readerCmdFile);	

	if (-1 == readerData[reader_index].commandFd)
	{
		printf("open %s: %s", readerCmdFile, strerror(errno));
		return STATUS_NO_SUCH_DEVICE;
	}

	if(InitializeSerial(reader_index, 9600, 8, 'E') != STATUS_SUCCESS)
		return STATUS_UNSUCCESSFUL;

	return STATUS_SUCCESS;
} /* OpenSerialByName */

status_t InitializeSerial(unsigned int reader_index, int baud, int bits, char parity) 
{

	int    handle = readerData[reader_index].readerFd;
	struct termios  newtio;

	/*
	 * Zero the struct to make sure that we don't use any garbage
	 * from the stack.
	 */
	memset(&newtio, 0, sizeof(newtio));

	/*
	 * Set the baudrate
	 */
	switch (baud) {

		case 9600:                                               
			cfsetispeed(&newtio, B9600);
			cfsetospeed(&newtio, B9600);
			break;
		case 19200:                              
			cfsetispeed(&newtio, B19200);
			cfsetospeed(&newtio, B19200);
			break;
		case 38400:                                           /* Baudrate 38400  */
			cfsetispeed(&newtio, B38400);
			cfsetospeed(&newtio, B38400);
			break;
		case 57600:                                           /* Baudrate 57600  */
			cfsetispeed(&newtio, B57600);
			cfsetospeed(&newtio, B57600);
			break;
		case 115200:                                          /* Baudrate 115200 */
			cfsetispeed(&newtio, B115200);
			cfsetospeed(&newtio, B115200);
			break;
		case 230400:                                          /* Baudrate 230400 */
			cfsetispeed(&newtio, B230400);
			cfsetospeed(&newtio, B230400);
			break;

		default:
			return STATUS_UNSUCCESSFUL;
	}

	/*
	 * Set the bits.
	 */
	switch (bits) {
		case 5:                                          
			newtio.c_cflag |= CS5;
			break;
		case 6:                                          
			newtio.c_cflag |= CS6;
			break;
		case 7:                                          
			newtio.c_cflag |= CS7;
			break;
		case 8:
			newtio.c_cflag |= CS8;
			break;
		default:
			return STATUS_UNSUCCESSFUL;
	}

	/*
	 * Set the parity (Odd Even None)
	 */
	switch (parity) {

		case 'O':                                             
			newtio.c_cflag |= PARODD | PARENB;
			newtio.c_iflag |= INPCK;  
			break;

		case 'E':                                          
			newtio.c_cflag &= (~PARODD); 
			newtio.c_cflag |= PARENB;
			newtio.c_iflag |= INPCK;  
			break;

		case 'N': 
			break;

		default:
			return STATUS_UNSUCCESSFUL;
	}

	/*
	 * Setting Raw Input and Defaults
	 */

	newtio.c_cflag |= CSTOPB;
	newtio.c_cflag |= CREAD|HUPCL|CLOCAL;
	newtio.c_iflag &= ~(IGNPAR|PARMRK|INLCR|IGNCR|ICRNL|ISTRIP);
	newtio.c_iflag |= BRKINT;  
	//        newtio.c_lflag = ~(ICANON|ECHO);
	newtio.c_oflag  = 0; 
	newtio.c_lflag  = 0;

	// put in remark because they dont seem to work
	//newtio.c_cc[VMIN]  = 0; // wait for any number of bytes arriving (first byte)
	//newtio.c_cc[VTIME] = 100; // 10 seconds timeout


	/* Set the parameters */	
	if (tcsetattr(handle, TCSANOW, &newtio) < 0) {   
		close(handle);
		return FALSE;
	}

	/*
	 * Wait while reader sends PNP information
	 * (I'll catch this information in future
	 *  versions if I get the information)
	 */

	usleep(1000000);

	if (tcflush(handle, TCIOFLUSH) < 0) {
		close(handle);
		return FALSE;
	}

	sleep(1);

	return STATUS_SUCCESS;
} /* InitializeSerial */

status_t SetBaudRate(unsigned int reader_index, unsigned int baud) 
{
	int    handle = readerData[reader_index].readerFd;
	struct termios  newtio;

	/*
	 * Zero the struct to make sure that we don't use any garbage
	 * from the stack.
	 */
	memset(&newtio, 0, sizeof(newtio));

	/* Get the parameters */	
	if (tcgetattr(handle, &newtio) < 0) {   
		close(handle);
		return FALSE;
	}

	/*
	 * Set the baudrate
	 */
	switch (baud) {

		case 9600:                                               
			cfsetispeed(&newtio, B9600);
			cfsetospeed(&newtio, B9600);
			break;
		case 19200:                              
			cfsetispeed(&newtio, B19200);
			cfsetospeed(&newtio, B19200);
			break;
		case 38400:                                           /* Baudrate 38400  */
			cfsetispeed(&newtio, B38400);
			cfsetospeed(&newtio, B38400);
			break;
		case 57600:                                           /* Baudrate 57600  */
			cfsetispeed(&newtio, B57600);
			cfsetospeed(&newtio, B57600);
			break;
		case 115200:                                          /* Baudrate 115200 */
			cfsetispeed(&newtio, B115200);
			cfsetospeed(&newtio, B115200);
			break;
		case 230400:                                          /* Baudrate 230400 */
			cfsetispeed(&newtio, B230400);
			cfsetospeed(&newtio, B230400);
			break;

		default:
			return STATUS_UNSUCCESSFUL;
	}

	/* Set the parameters */	
	if (tcsetattr(handle, TCSANOW, &newtio) < 0) {   
		close(handle);
		return STATUS_UNSUCCESSFUL;
	}
	return STATUS_SUCCESS;
}

status_t OpenSerial(unsigned int reader_index, unsigned int Channel)
{
	int Unibble = (Channel & 0xFFFF0000) >> 16;         /* Upper 16bits of DWORD. (XXXX - Reader) */

	if(Unibble > TI_DRIVER_MAX_READERS) {
		printf("wrong port number: %d", (int) Channel);
		return STATUS_UNSUCCESSFUL;
	}

	(void)sprintf(readerData[reader_index].readerName, TI_READER_NAME, Unibble);

	return OpenSerialByName(reader_index, readerData[reader_index].readerName);
} /* OpenSerial */

status_t ReadSerial(unsigned int reader_index, unsigned int *length, unsigned char *buffer)
{
	int    handle = readerData[reader_index].readerFd;
	int    rval;
	int    readBytesNum = 0;
	struct timeval  tv;
	fd_set rfds;

	do {
		tv.tv_sec  = 0;
		tv.tv_usec = 1000000;
		FD_ZERO(&rfds);
		FD_SET(handle, &rfds);

		rval = select(handle+1, &rfds, NULL, NULL, &tv);

		if ((rval == -1) || (rval == 0)) {
			break;
		}

		if((rval = read(readerData[reader_index].readerFd, buffer+readBytesNum, *length-readBytesNum)) == -1)
			return STATUS_UNSUCCESSFUL;

		readBytesNum += rval;

	} while(readBytesNum < *length);

	*length = readBytesNum;

	// TDB Delete the following debug code later
	{
		int i = 0;
		printf ("TI IFD HANDLER UTILS ReadSerial RX  BYTES - %d  DATA - ", *length);
		for( i = 0; i < *length; i++)
			printf("%02X ", buffer[i]);
		printf("\n");
	}
	if(*length == 0)
		return STATUS_UNSUCCESSFUL;

	return STATUS_SUCCESS;
} /* ReadSerial */

status_t ReadSerialNoWait(unsigned int reader_index, unsigned int *length, unsigned char *buffer)
{
	int    handle = readerData[reader_index].readerFd;
	int    rval;
	int    readBytesNum = 0;
	struct timeval  tv;
	fd_set rfds;

	do {
		tv.tv_sec  = 0;
		tv.tv_usec = 0;
		FD_ZERO(&rfds);
		FD_SET(handle, &rfds);

		rval = select(handle+1, &rfds, NULL, NULL, &tv);

		if ((rval == -1) || (rval == 0)) {
			break;
		}

		if((rval = read(readerData[reader_index].readerFd, buffer+readBytesNum, *length-readBytesNum)) == -1)
			return STATUS_UNSUCCESSFUL;

		readBytesNum += rval;

	} while(readBytesNum < *length);

	*length = readBytesNum;

	// TDB Delete the following debug code later
	{
		int i = 0;
		printf ("TI IFD HANDLER UTILS ReadSerial RX  BYTES - %d  DATA - ", *length);
		for( i = 0; i < *length; i++)
			printf("%02X ", buffer[i]);
		printf("\n");
	}
	if(*length == 0)
		return STATUS_UNSUCCESSFUL;

	return STATUS_SUCCESS;
} /* ReadSerialNoWait */


status_t WriteSerial(unsigned int reader_index, unsigned int length, unsigned char *buffer)
{
	int len = 0, remain = 0, offset = 0;;

	// TDB Delete the following debug code later
	{
		int i = 0;
		printf ("TI IFD HANDLER UTILS WritSerial TX  BYTES - %d  DATA - ", length);
		for( i = 0; i < length; i++)
			printf("%02X ", buffer[i]);
		printf("\n");
	}

	remain = length;

	while(remain) {
		if((len = write(readerData[reader_index].readerFd, buffer+offset, remain)) < 0)
			return STATUS_UNSUCCESSFUL;

		remain -= len;
		offset += len;
	}

	if(offset != length)
		return STATUS_UNSUCCESSFUL;

	return STATUS_SUCCESS;
} /* WriteSerial */

status_t CloseSerial(unsigned int reader_index)
{
	/* device not opened */
	if ( -1 == readerData[reader_index].readerFd)
		return STATUS_UNSUCCESSFUL; 

	printf("Closing serial device: %s\n", readerData[reader_index].readerName);


	(void)close(readerData[reader_index].readerFd);
	readerData[reader_index].readerFd  = -1;

	return STATUS_SUCCESS;
} /* CloseSerial */     

